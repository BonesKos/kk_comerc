# jun/01/2023 20:05:07 by RouterOS 6.49.8
# software id = X8DN-FLM1
#
# model = CCR1009-8G-1S-1S+
# serial number = 48FF048648AC
# Правило разрешения обмена KK COMERC DST-NAT разместить выше запрещающих. 
/ip firewall filter
add action=add-dst-to-address-list address-list=KK_enableVZLET_COMERC \
    address-list-timeout=none-dynamic chain=input comment=\
    "KK_enableCOMERC_VZLET PING ENABLE" dst-address=192.168.4.10 protocol=\
    icmp src-address=192.168.4.51
add action=add-dst-to-address-list address-list=KK_enableCOMERC \
    address-list-timeout=none-dynamic chain=input comment=\
    "KK COMERC PING ENABLE" dst-address=192.168.4.10 protocol=icmp \
    src-address=192.168.4.115
add action=accept chain=forward comment="KK COMERC DST-NAT" \
    connection-nat-state=dstnat dst-address=88.147.165.126 dst-port=5001 \
    protocol=tcp
add action=accept chain=forward comment="KK COMERC DST-NAT" \
    connection-nat-state=dstnat dst-address=88.147.164.94 dst-port=5001 \
    protocol=tcp
/ip firewall nat
add action=masquerade chain=srcnat comment="KK LOCAL masquer" \
    src-address-list=LOCAL
add action=dst-nat chain=dstnat comment="KK TECH ON" dst-address=192.168.4.10 \
    dst-port=1 protocol=tcp to-addresses=88.147.165.126 to-ports=5001
add action=dst-nat chain=dstnat comment="KK COMERC ON" disabled=yes \
    dst-address=192.168.4.10 dst-port=2 protocol=tcp to-addresses=\
    88.147.165.126 to-ports=5001
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41011 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41011
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41021 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41020
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41041 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41040
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41071 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41070
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41121 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41120
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41131 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41130
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41141 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41140
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41211 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41210
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41221 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41220
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41241 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41240
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41251 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41250
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41271 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41270
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41281 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41280
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41291 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41290
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41301 protocol=tcp to-addresses=88.147.164.94 \
    to-ports=5001
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41311 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41310
add action=dst-nat chain=dstnat comment="KK VZLET-TECH" dst-address=\
    192.168.4.10 dst-port=41371 protocol=tcp to-addresses=192.168.4.7 \
    to-ports=41370
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41012 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41010
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41022 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41020
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41042 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41040
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41072 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41070
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41122 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41120
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41132 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41130
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41142 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41140
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41212 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41210
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41222 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41220
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41242 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41240
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41252 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41250
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41272 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41270
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41282 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41280
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41292 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41290
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41302 protocol=tcp to-addresses=\
    88.147.164.94 to-ports=5001
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41312 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41310
add action=dst-nat chain=dstnat comment="KK VZLET-COMERC" disabled=yes \
    dst-address=192.168.4.10 dst-port=41372 protocol=tcp to-addresses=\
    192.168.4.7 to-ports=41370
/system script
add dont-require-permissions=no name=kk_comerc_vzlet owner=KK policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon source=":\
    local t [/ip firewall address-list find where list=\"KK_enableVZLET_COMERC\
    \"]\
    \n\r\
    \n:if (\$t != \"\") do={\
    \n    \r\
    \n/ip firewall filter  disable [find comment=\"KK_enableCOMERC_VZLET PING \
    ENABLE\"]\
    \n    \r\
    \n   \
    \n    \r\
    \n:put \"KK_enableVZLET_COMERC\"\
    \n    \r\
    \n:log info message=\"KK_enableVZLET_COMERC\"\
    \n   \r\
    \n/ip firewall address-list remove \$t\r\
    \n\
    \n\r\
    \n/ip firewall nat enable [find comment=\"KK VZLET-COMERC\"]\
    \n    \r\
    \n/ip firewall nat disable [find comment=\"KK VZLET-TECH\"]\
    \n    \r\
    \n/ip firewall connection remove [find where dst-address =\"192.168.4.10:4\
    1301\"]\
    \n \r\
    \n:delay 60\
    \n    \r\
    \n#:delay 1800\
    \n    \r\
    \n:put \"KK KK_enableVZLET_COMERC - DISABLE\"\
    \n    \r\
    \n:log info message=\"KK KK_enableVZLET_COMERCDISABLE\"\
    \n    \r\
    \n/ip firewall nat enable [find comment=\"KK VZLET-TECH\"]\
    \n    \r\
    \n/ip firewall nat disable [find comment=\"KK VZLET-COMERC\"]\
    \n    \r\
    \n/ip firewall filter enable [find comment=\"KK_enableCOMERC_VZLET PING EN\
    ABLE\"]\r\
    \n/ip firewall connection remove [find where dst-address =\"192.168.4.10:4\
    1302\"]\
    \n \r\
    \n \
    \n\
    \n\
    \n} else={\
    \n    :put \"no address-list find KK_enableVZLET_COMERC\"\r\
    \n\
    \n}\
    \n"
add dont-require-permissions=no name=kk_comerc owner=KK policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon source=":\
    local t [/ip firewall address-list find where list=\"KK_enableCOMERC\"]\
    \n:if (\$t != \"\") do={\
    \n    /ip firewall filter  disable [find comment=\"KK COMERC PING ENABLE\"\
    ]\
    \n      \
    \n    :put \"KK COMERC ENABLE\"\
    \n  /ip firewall address-list remove \$t   :log info message=\"KK COMERC E\
    NABLE\"\
    \n    /ip firewall nat enable [find comment=\"KK COMERC ON\"]\
    \n    /ip firewall nat disable [find comment=\"KK TECH ON\"]\
    \n    /ip firewall connection remove [find where dst-address =\"192.168.4.\
    10:1\"]\
    \n    :delay 60\
    \n    #:delay 1800\
    \n    :put \"KK COMERC DISABLE\"\
    \n    :log info message=\"KK COMERC DISABLE\"\
    \n    /ip firewall nat enable [find comment=\"KK TECH ON\"]\
    \n    /ip firewall nat disable [find comment=\"KK COMERC ON\"]\
    \n    /ip firewall connection remove [find where dst-address =\"192.168.4.\
    10:2\"]\
    \n    /ip firewall filter enable [find comment=\"KK COMERC PING ENABLE\"]\
    \n\
    \n \r\
    \n\
    \n  } else={\
    \n    :put \"adress-list KK_enableCOMERC is empty\"\
    \n}\
    \n"
/system scheduler
add interval=10s name=kk_comerc-sch on-event=\
    "/system script run kk_comerc_vzlet\r\
    \n/system script run kk_comerc\r\
    \n" policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon \
    start-date=jun/01/2023 start-time=19:35:51
